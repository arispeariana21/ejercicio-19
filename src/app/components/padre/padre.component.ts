import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
  Zayn!:any;
  Niall!:any;

  constructor() { }

  ngOnInit(): void {
  }

  recibirObjeto1($event:any):void{
    this.Zayn = $event;
    console.log(this.Zayn);
  }

  recibirObjeto2($event:any):void{
    this.Niall = $event;
    console.log(this.Niall);
    
  }
}
